# -*- py-which-shell: "python2.7" -*-

"""
This is a regular expression engine for sequences of objects rather
than characters.

Right now the codebase is a little entwined in what I've been working
on for a linguistics project, but after that project settles down I
hope to decouple the generic parts of this from those specific to the
objects you're matching against.

As far as the Matcher class goes, you'd just have to redfine the init
and matcher methods in TokenMatcher to use the engine (not the search
string compiler mind you) for some other kind of object. TokenMatcher
is the only part that actually touches the objects you're trying to
match. Everybody else is just delegating and passing generators
around.

I'm not saying this is a solution, just saying that the coming up with
one shouldn't be a huge thing.

More challenging but equally, if not more enticing is abstracting the
search expression compiler, leaving the standard regex operators alone
but making it easy to bind a bunch of crazy syntax to a bunch of crazy
object-matching functions. You know like, on the level of a character
literal in a text regex.

Another thing that's kind of ugly is all this checking and evaluating
callables for lazy evaluation, which seems necessary for recursive
macro definitions.


TODO:
  - Handle empty sequences.
  - Character escaping.
  - Match anchors: ^ and $ (maybe)
  - Think about best syntax for macro symbols. It's currently $SYMBOL.

"""

from __future__ import print_function

import abc
import os.path
import unittest
import tests
from collections import namedtuple
from functools import reduce

# a mock token
TinyToken = namedtuple("TinyToken", "lemma pos features")

def make_token(token_string):
    """ Creates a 'word' for an 'utterance'

    token_string is in the format 'word/POS/FEATURE1:FEATURE2'

    where features are optional and are basically parts of speech you
    think aren't as important as what you picked for POS
    """
    try:
        lemma, pos, features = token_string.split("/")
        features = features.split(":")
    except ValueError:
        lemma, pos = token_string.split("/")
        features = []
    return TinyToken(lemma, pos, features)


def make_utterance(utterance_string):
    """ Returns a list of TinkTokens """

    return [make_token(t) for t in utterance_string.split(" ")]

class TokenCompilerException(Exception):
    pass

class MatchObject(object):
    """ Gets passed around by Matchers when they've found a
    match. token_tail is a list of the tokens that were not consumed
    by the match  """

    def __init__(self, token_tail):
        self.token_tail = token_tail

    def __repr__(self):
        return "MatchObject(tail_len=%s)" %len(self.token_tail)

    @property
    def tail_len(self):
        return len(self.token_tail)

class Matcher:
    """ The base class all matchers inherit from.

    All Matcher classes must have an init and match method.

    For all generic matchers (all those other than TokenMatcher) their
    __init__ must accept zero or more Matchers or callable objects.

    Their .match method must accept a list of tokens, and yield a
    MatchObject for each match that's found by the matchers it was
    passed as an argument. The match object's token_tail property will
    contain the tokens not consumed by the match- that is,
    len(token_tail) = len(tokens) - len(length-of-match)

    Additionally, the .match function must check each of its matcher
    vars for callable-ness. If one is callable, it should be
    evaluated, and the result from this evaluation is the matcher to
    be used. This lazy evaluation is necessary to prevent endless
    macro expansions.

    """

    __metaclass__ = abc.ABCMeta

    def __repr__(self):
        args = ["%s=%s" % (name, val) for name, val in self.__dict__.items()]
        return str(self.__class__.__name__) + "(" + " ".join(args) + ")"

    def search(self, tokens):
        """ Returns true for 'substring match' anywhere in the token
        list """

        head = ZeroOrMoreMatcher(self)
        return(any(m for m in self.match(tokens)))

    def full_match(self, tokens):
        """ Returns true if matches start to finish"""
        return(any(m.tail_len == 0 for m in self.match(tokens)))

    @abc.abstractproperty
    def match(self, tokens):
        return

class WildCardMatcher(Matcher):
    """ Returns true for any token. Consumes one token. """
    def match(self, tokens):
        yield MatchObject(tokens[1:])

class RangeMatcher(Matcher):
    """ Abstract reptitive Matcher. Currently only supports maximum
    range.  """


    def __init__(self, matcher=None):
        if isinstance(matcher, RangeMatcher):
            raise Exception("Nothing to Repeat")

        self.matcher = matcher

    def _find_shortest_tail(self, tokens, max_reps, reps=0):
        """ Finds the longest match which thus yields the shortest
        token_tail."""

        if reps == max_reps:
            return len(tokens)
        try:
            return min(self._find_shortest_tail(m.token_tail,
                                                max_reps,
                                                reps + 1)
                       for m in self.matcher.match(tokens))
        except ValueError:  # if we give max an empty sequence
            return len(tokens)

    def match(self, tokens, max_rep, empty=False):
        """ Matches on at most max_rep repitions of self.matcher. If
        empty=False it will match on zero reps.

        """

        if callable(self.matcher):
            self.matcher = self.matcher()
        shortest_tail = self._find_shortest_tail(tokens, max_rep)
        longest_match_len = len(tokens) - shortest_tail
        # backtracking
        for i in range(longest_match_len, 0, -1):
            for m in self.matcher.match(tokens[:i]):
                if m:
                    yield MatchObject(tokens[i:])
        if empty is True:
            yield MatchObject(tokens)

class ZeroOrMoreMatcher(RangeMatcher):
    def match(self, tokens):
        return RangeMatcher.match(self, tokens, len(tokens), empty=True)

class ZeroOrOneMatcher(RangeMatcher):
    def match(self, tokens):
        return RangeMatcher.match(self, tokens, 1, empty=True)

class OneOrMoreMatcher(RangeMatcher):
    def match(self, tokens):
        return RangeMatcher.match(self, tokens, len(tokens))


class GroupMatcher(Matcher):
    """ Matches if any matcher in matchers matches """
    def __init__(self, *matchers):
        for m in matchers:
            assert(isinstance(m, Matcher) or callable(m))
        self.matchers = matchers

    def match(self, tokens):
        for matcher in self.matchers:
            matcher = matcher() if callable(matcher) else matcher
            for m in matcher.match(tokens):
                yield m

class InvertedGroupMatcher(Matcher):
    """ Fails to match if any matchers in matchers matches. """
    def __init__(self, *matchers):
        for m in matchers:
            assert(isinstance(m, Matcher) or callable(m))
        self.matchers = matchers

    def match(self, tokens):
        any_match = False
        for matcher in self.matchers:
            matcher = matcher() if callable(matcher) else matcher
            for m in matcher.match(tokens):
                any_match = True
                break
        if not any_match:
            yield MatchObject(tokens[1:])

class ConcatenatedMatcher(Matcher):
    """ Takes two matchers and matches if m1 and m2 match in
    succession. """

    def __init__(self, m1, m2):
        self.first_matcher = m1
        self.second_matcher = m2

    def match(self, tokens):
        if callable(self.first_matcher):
            self.first_matcher = self.first_matcher()
        if callable(self.second_matcher):
            self.second_matcher = self.second_matcher()

        for a in self.first_matcher.match(tokens):
            try:
                for b in self.second_matcher.match(a.token_tail):
                    yield b
            except IndexError:
                # If matcher a exhausts all the tokens, b will raise
                # exception because I'm not handling empty sequence
                # matches yet. It's on the todo list.
                continue

class ConcreteMatcher:
    __metaclass__ = abc.ABCMeta

    @abc.abstractproperty
    def __init__():
        raise NotImplementedError()

class FilteredMatcher(ConcreteMatcher):
    def __init__(self, a, b):
        pass


class TokenMatcher(Matcher):
    """ Matches against 'object literals'.

    This is the only code that actually touches the objects being
    matched against. Right now this is hard coded for this linguistics
    project, but in the future this would be what you'd extend to
    match against objects of your own.

    """

    def __init__(self, pos_list=None, lemma=None, wordform=None, exclude_pos=None):
        self.lemma = lemma
        self.pos_list = [] if pos_list is None else pos_list
        self.wordform = wordform
        self.exclude_pos = [] if exclude_pos is None else exclude_pos
        if len(set(self.pos_list).intersection(set(self.exclude_pos))) > 0:
            raise Exception("excluded features cannot contain POS")

    def __repr__(self):
        return ("Matcher(lemma=%s, wordform=%s, pos_list=%s,"
                " exclude_pos=%s)") % (self.lemma.__repr__(),
                                       self.wordform.__repr__(),
                                       self.pos_list.__repr__(),
                                       self.exclude_pos.__repr__())
    def match(self, tokens):
        token = tokens[0]
        fail = False
        if self.wordform is not None:
            if token.wordform is None:
                fail = True
            # unfortunately this has to be case-insensitive
            elif self.wordform.lower() != token.wordform.lower():
                fail = True
        if self.lemma is not None:
            if self.lemma != token.lemma:
                fail = True
        for pos in self.pos_list:
            if token.features is None:
             if pos.lower() != token.pos.lower():
                 fail = True
            else:
                at_least_one_match = False
                for other_pos in token.features + [token.pos]:
                    if pos.lower() == other_pos.lower():
                        at_least_one_match = True
                if at_least_one_match is False:
                    fail = True
        for feature in self.exclude_pos:
            if token.features:
                if feature.lower() in [i.lower() for i in token.features]:
                    fail = True
        if token.pos.lower() in [i.lower() for i in self.exclude_pos]:
            fail = True
        if not fail:
            yield MatchObject(tokens[1:])


################################################################################
##                                                                            ##
##                REGULAR EXPRESSION PARSING AND COMPILATION                  ##
##                                                                            ##
################################################################################

def make_matcher(match_string, definitions=None, cache={}):
    """ Takes a regular expression match string and returns a matcher
    object.


    WARNING: The definitions dict is not being cached, so passing in a
    different the same match_string with different definitions dict
    will probably lead to incorrect behavior.

    """

    try:
        return cache[match_string]
    except KeyError:
        tokens = tokenize_match_string(match_string)
        regex = compile_regex(tokens, definitions)
        cache[match_string] = regex
        return regex


def tokenize_match_string(match_string):
    """ Takes a string and returns a list of tokens. """
    stack = []
    token_seperators = "()|?*+^ "
    for char in match_string:
        if char in token_seperators:
            if len(stack) > 0:
                yield "".join(stack)
            if char != ' ':  # drop spaces
                yield char
            stack = []
        else:
            stack.append(char)
    if len(stack) > 0:
        yield "".join(stack)

def pop(stack):
    """ Pops the entire stack and returns a string concatenation of
    it.
    """

    ret = []
    while len(stack) > 0:
        ret.append(stack.pop())
    return "".join(ret)

def compile_token_matcher_expression(match_token):
    """ Accepts a single 'object-literal' match-token-string and
    returns a match object.

    Here are the components of a MOR 'object literal'

    lowercasetext (1 or 0)
        a word's lemma

    `lowercasetext (1 or 0)
        the word's literal wordform

    UPPERCASETEXT (1 or 0)
        the part of speech to match against

    &UPPERCASETEXT (1 or many)
        additional parts of speech to match against in the feature
        list

    !UPPERCASETEXT (1 or many)
        a part of speech that will cause a failure to match

    A well-formed match token must contain one or more of the
    components above, and they must occur in the order listed. A token
    may not begin with a &UPPERCASE component.

    """

    lemma = None
    wordform = None
    stack, pos, excludes = [], [], []
    upper = match_token[-1].isupper()
    consuming_wordform = False
    for char in match_token[::-1]:
        if char == "&":
            if upper is False:
                raise TokenCompilerException("Invalid token", match_token)
            pos.append(pop(stack))
        elif char == "!":
            if upper is False:
                raise TokenCompilerException("Invalid token", match_token)
            excludes.append(pop(stack))
        elif char == "`":
            if upper is True or wordform is not None:
                raise TokenCompilerException("Invalid token", match_token)
            else:
                wordform = pop(stack)
        elif not char.isalpha() and char !="'":
            raise TokenCompilerException("Invalid token", match_token)
        elif upper and char.islower():
            pos.append(pop(stack))
            stack.append(char)
            upper = False
        else:
            stack.append(char)
    left = pop(stack)
    if left.isupper():
        pos.append(left)
    elif left.islower():
        assert(lemma is None)
        lemma = left
        if "'" in lemma:
            raise TokenCompilerException("' not permitted in lemma: %s" %
                                         match_token)
    return TokenMatcher(lemma=lemma, wordform=wordform,
                        pos_list=pos, exclude_pos=excludes)


def compile_token(match_token, definitions=None):
    """ Takes a match token (as described in
    compile_token_matcher_expression) or a macro (of the form
    $UPPERCASE) and returns a matcher.

    definitions is a dictionary of NAME:search-string pairs. if a
    token of the form $X is encountered, definitions[X] will be
    compiled. if definitions[X] contains another macro, a function is
    returned that when called compiles definitions[X], thus
    compilation is defered until a MatchObjects match method is
    called.

    """

    definitions = {} if definitions is None else definitions

    if match_token.startswith("$"):
        try:
            macro_string = definitions[match_token[1:]]
            if macro_string.lstrip().startswith(match_token):
                raise Warning("Defining infinitely recursive macro %s = %s" %
                              (match_token, macro_string))
        except KeyError:
            raise TokenCompilerException("%s has not been defined" % match_token)
        if "$" in macro_string:
            # the macro contains a macro. defer further macro
            # expansion to avoid cyclic expansion
            return lambda: make_matcher(definitions[match_token[1:]], definitions)
        else:
            return make_matcher(definitions[match_token[1:]], definitions)

    if match_token in ".,?!":
        return TokenMatcher(wordform=match_token)

    return compile_token_matcher_expression(match_token)


def get_repeater(match_token):
    if match_token == "*":
        return ZeroOrMoreMatcher
    if match_token == "?":
        return ZeroOrOneMatcher
    if match_token == "+":
        return OneOrMoreMatcher
    raise Exception("Unknown token %s" % match_token)

def compile_group(stack, implicit_group=False):
    """ Compiles a matcher from a subexpression inside a () or (^)
    group."""

    args = []
    concat = True
    while True:
        try:
            item = stack.pop()
        except IndexError as e:
            if implicit_group:
                stack.append(GroupMatcher(*args))
                break
            else:
                raise e
        if item is GroupMatcher:
            stack.append(GroupMatcher(*args))
            break
        if item is InvertedGroupMatcher:
            stack.append(InvertedGroupMatcher(*args))
            break
        if item != "|":
            if concat:
                try:
                    args.append(ConcatenatedMatcher(item, args.pop()))
                except IndexError:
                    args.append(item)
            else:
                args.append(item)
            concat = True
        else:
            concat = False

def compile_regex(search_tokens, definitions=None):
    """ Takes a list of match tokens and returns a Matcher Object"""

    stack = []
    for token in search_tokens:
        if token == "(":
            stack.append(GroupMatcher)
        elif token == ")":
            compile_group(stack)
        elif token == "^":
            assert(stack.pop() is GroupMatcher)
            stack.append(InvertedGroupMatcher)
        elif token == ".":
            stack.append(WildCardMatcher())
        elif token in "*?+":
            repeater = get_repeater(token)
            stack.append(repeater(stack.pop()))
        elif token == "|":
            stack.append(token)
        else:
            stack.append(compile_token(token, definitions))
    compile_group(stack, implicit_group=True)
    assert(len(stack) == 1)
    return stack[0]

def concat(*matchers):
    """ A convenience method for concatenating multiple matchers. """
    return reduce(ConcatenatedMatcher, matchers)

if __name__ == "__main__":
    unittest.main(tests)


