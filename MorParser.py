#!/usr/bin/env python2.7
# -*- py-which-shell: "/opt/bin/python2.7"; -*-

""" Tagset/format conversion and preprocessing on MOR+POST tagged
files.

run script to see cli usage.

"""

from __future__ import print_function
import argparse
import csv
import glob
import os
import re
import math
import string
import sys
from pprint import pprint
from contextlib import nested

DEBUG = False


def collapse_lines(cont_delim, lines):
    """ Returns a generator that returns lines from _lines_ where all
    lines starting with cont_delim are space-appended to the line
    before them"""

    line_buffer = []
    for num, line in enumerate(lines):
        if not line.startswith(cont_delim):
            # flush the buffer
            yield num, " ".join(line_buffer)
            line_buffer = []
        line_buffer.append(line.strip("\n" + cont_delim))
    yield num, " ".join(line_buffer)

class NullWordformError(Exception):
    def __init__(self, tag, message):
        Exception.__init__(self, tag)
        InvalidTagError.broken_tags.append(tag)

class InvalidTagError(Exception):
    """ Thrown when unparsable tags are found """
    broken_tags = []
    def __init__(self, tag, message):
        Exception.__init__(self, tag)
        InvalidTagError.broken_tags.append(tag)

class TagError(Exception):
    def __init__(self, message, Errors):
        Exception.__init__(self, message)
        self.Errors = Errors

class MorToken(object):

    known_punctuation = [',', '.', '?', '!', '+/.', '+...', '+"/.', '+//.', '+/?']
    unknown_punctuation = ['+".', '+...?', '+..?', '+!?'] # need to check on these.
    punctuation = known_punctuation + unknown_punctuation

    def __init__(self, tag, wordform=None, is_enclitic=False, translator=None):

        self.translator = translator

        # strings
        self.prefix = None
        self.pos = None
        self.lemma = None
        self.wordform = wordform
        self.source = tag

        # lists
        self.features = None
        self.fusions = None
        self.suffixes = None

        # MorToken
        self.enclitic = None

        self.parse_tag(tag)

        if not is_enclitic:
            self.massage_enclitics()

    @property
    def tpos(self):
        if self.translator:
            for token, rewrite in self.translator:
                match = True
                for key, val in token.items():
                    match = (self[key] == val) and match
                    # print(key.upper(), self[key], "=" if self[key] == val else "!=", val)
                if match:
                    # print("\n\t", self.source, "--->", rewrite.upper())
                    # print(token, rewrite)
                    # print("RESULT:",  result)
                    return rewrite
            else:
                return self.pos
        else:
            return self.pos

    def has_data(self):
        return (not all([i == "" for i in [self.pos, self.lemma, self.source]])) \
               or "|" in self.lemma

    def has_enclitic(self):
        return self.enclitic

    def parse_tag(self, text, wordform=None, sourceLine=None):
        """ Breaks a mor token up into its components

        Args:
            `text`: a string containing a single mor token. for example,
                'aux|can', 'pro:poss:det|your', 'adj:n|poop-Y' are all
                parsable mor strings.

        Returns:
            a dictionary with the following keys:

                - prefix
                - pos (part of speech)
                - features (list)
                - lemma
                - wordform
                - suffix (list)
                - fusion (list)
                - enclitic (a mor tag dictionary)
                - source (the original mor token)

        if the string parses, the 'pos', 'lemma' and 'source' keys
        must have non-None values. 'source' contains the original mor token,
        passed as an argument (aka `text`).

        Will raise ParseNotFoundError if no parse is found

        """

        # TODO: update the following for compounds and enclitics

        # mor string format:
        #    [prefix#]pos[:feature1][:feature2]...[:featureN]|word[&fusion1]...[&fusionN][-suffix1]...[-suffixN]

        # where '#', ':', '|', '&' and '-' are literal, and [ ] denotes optionality

        if text in MorToken.punctuation:
            # punctuation
            self.wordform = self.text = self.source = self.pos = self.lemma = text
            return

        parts = text.split("~")
        base = parts[0]
        enclitic = parts[1] if len(parts) > 1 else None
        if text.count("+") > 1:
            pos, rest = re.split("\|", text, 1)
            tagmatch = re.match("(.+?)((?P<fusions>(&.*?)+))?" \
                               "((?P<suffixes>(-[A-Z0-9]*?)+))?$", rest)
            md = tagmatch.groupdict()
            compound_words = tagmatch.group(1)
            self.pos = pos
            if md['fusions']:
                self.fusions = [f for f in re.findall("&(\w+)", md['fusions'])]
            if md['suffixes']:
                self.suffixes = [f for f in re.findall("-(\w+)", md['suffixes'])]
            # --> n|+n|(tape)+n|(recorder)
            self.lemma = "".join(re.findall("\|([^\+]+)", compound_words))

        else:
            tagmatch = re.match("((?P<prefix>.+?)#)?" \
                            "(?P<pos>.+?)" \
                              "(:(?P<features>(.+?)))?\|" \
                              "(?P<lemma>[^\|]+?)" \
                              "((?P<fusions>(&.*?)+))?((?P<suffixes>(-[A-Z0-9]*?)+))?$",
                              base)
            if not tagmatch:
                raise InvalidTagError(text, "")
            md = tagmatch.groupdict()
            self.__dict__.update(md)

            if md['features']:
                self.features = [f for f in md['features'].split(":")]
            if md['fusions']:
                self.fusions = [f for f in re.findall("&(\w+)", md['fusions'])]
            if md['suffixes']:
                self.suffixes = [f for f in re.findall("-(\w+)", md['suffixes'])]

        if enclitic:
            self.enclitic = MorToken(enclitic, is_enclitic=True, translator=self.translator)

        if not self.has_data:
            raise InvalidTagError(text, "")

    def massage_enclitics(self):
        """ expands contracted and possesive words

        args:
          `token`: a MorToken

        returns:
          a list of mor tag dicts. if no possesion or contraction is
          found, there will be a single item in the list.


        this is currently mutating the token argument

        tokenization algorithm taken from:
            http://www.cis.upenn.edu/~treebank/tokenization.html
        """

        # posessive
        if not self.has_enclitic() and self.suffixes and 'POSS' in self.suffixes:
            try:
                word, mark = re.split("'", self.wordform)
            except ValueError:
                raise ValueError(self.wordform, self)
            self.wordform = word
            self.enclitic = MorToken("POSS|'%s" % mark, "'" + mark, is_enclitic=True)

        elif self.has_enclitic(): # contraction
            tails = ["('ll)", "('re)", "('ve)", "(n't)", "('LL)",
                     "('RE)", "('VE)", "(N'T)", r"('[sSmMdD])"]

            unmarked = [["([Cc])annot", r"\1na not"],
                        ["([Dd])'ye", r"\1' ye"],
                        ["([Gg])imme", r"\1im me"],
                        ["([Gg])onna", r"\1on na"],
                        ["([Gg])otta", r"\1ot ta"],
                        ["([Ll])emme", r"\1em me"],
                        ["([Mm])ore'n", r"\1or 'n"],
                        ["'([Tt])is", r"'\1 is"],
                        ["'([Tt])was", r"'\1 was"],
                        ["([Ww])anna", r"\1an na"]]

            encliticsFound = 0
            for pattern in tails + [pat for pat, word in unmarked]:
                if re.search(pattern, self.wordform):
                    encliticsFound += 1
            del word

            if encliticsFound > 1:
                raise Exception("multiple enclitic splits found", self)

            if encliticsFound:
                for tail in tails:
                    if re.search(tail, self.wordform):
                        pair = re.split(tail, self.wordform)
                for pattern, rewrite in unmarked:
                    if re.search(pattern, self.wordform):
                        pair = re.sub(pattern, rewrite, self.wordform).split(' ')
                self.wordform = pair[0]
            #     enclitic = token['enclitic']
                self.enclitic.wordform = pair[1]
            #     token['enclitic'] = None
        if self.wordform == None:
            pprint(self.__repr__())
            print(self.source)
            raise NullWordformError(self, self.source)
        # else:
        #     try:
        #         if re.search("'s", self.wordform):
        #             raise TagError("posessive/enclitic not tagged: %s" % self.wordform, self.wordform)
        #     except:
        #         print(self.__repr__())
        # if enclitic is not None:
        #     return [token, enclitic]
        # else:
        #     return [token]


    def translate(self, equiv_dict):
        for token, rewrite in equiv_dict:
            match = True
            for key, val in token.items():
                match = (self[key] == val) and match
                # print(key.upper(), self[key], "=" if self[key] == val else "!=", val)
            if match:
                # print("\n\t", self.source, "--->", rewrite.upper())
                # print(token, rewrite)
                # print("RESULT:",  result)
                result = "%s/%s" % (self.wordform, rewrite)
                break
        else:
            result = "%s/%s" % (self.wordform, self['pos'])

        return result

    def __getitem__(self, key):
        return self.__dict__[key]

    def __setitem__(self, key, val):
        try:
            if isinstance(val, type(self.__dict__[key])):
                self.__dict__[key] = val
            else:
                print(self.__dict__)
                raise TypeError("invalid type %s (%s) for MorToken key '%s'"
                                % (type(val), val, key))
        except KeyError:
            raise KeyError("invalid key for MorToken %s" % key)

    def __str__(self):
        val = string.Template("$wordform/$pos").substitute(self.__dict__)
        return val

    def __repr__(self):
        return "MorToken(" + ", ".join(["%s=%s" % (key, val.__repr__())
                for key, val in self.__dict__.items()]) + ")"

    def __eq__(self, other):
        if type(other) == type(self):
            return self.__dict__ == other.__dict__
        elif type(other) == dict:
            equal = (sorted(other.keys()) == sorted(self.__dict__.keys()))
            for key, val in other.items():
                equal = equal and (val == self.__dict__[key])
            return equal
        else:
            raise Exception()


class MorSentence(object):
    """ Represents a single utterance """
    def __init__(self, utternace, morline, translator=None, strip_punctuation=False):
        self.tokens = [MorToken(wordform, token, translator=translator)
                       for token, wordform in self.tokenize(utternace, morline)]
        self.translator = translator
        self.strip_punctuation = strip_punctuation

        expandedTokens = []
        for token in self.tokens:
            expandedTokens.append(token)
            if token.enclitic is not None:
                expandedTokens.append(token.enclitic)

        self.tokens = expandedTokens


    def tokenize(self, utterance, tags):
        """ extracts (word, tag) pairs from an utterance and the words
        tagged from that utterance in order to pair wordforms with their
        mor tags.

        args:
          `utterance`: a string from MOR's speaker tier. can contain all
                       those crazy annotation markings, which will be
                       stripped.
          `tags`: the line produced from utterance passing through MOR and
                  POST

        Returns:
          a list of tuples (word, tag) pairing each tag with its wordform.

        """

        rawLine = sanitize_speaker_tier(utterance, debug=False)
        # rawLine = re.sub(',( ,)+', ',', rawLine)
        rawLine = re.sub(' +,', ',', rawLine)
        morLine = (re.sub("^%mor:[\s]+?", "", tags)).rstrip(' \n')

        rawList = rawLine.split()
        morList = morLine.split()

        # commas are currently concatenated with speaker tier text. here,
        # we put spaces between words and commas on the speaker tier and
        # insert commas in their matching locations on the mor tier.

        # iterate backwards through list. need index num to mutate list items.
        for index in range(len(rawList) - 1, -1, -1):
            if ',' in rawList[index]:
                rawList[index] = re.sub(",", "", rawList[index])
                rawList.insert(index + 1, ',')
                morList.insert(index + 1, ',')

        for wordform, token in zip(rawList, morList):
            try:
                yield wordform, token
            except NullWordformError:
                raise NullWordformError(rawList, morList)

    def __iter__(self):
        for t in self.tokens:
            yield t

    def __str__(self):
        if self.translator:
            return "%s" % " ".join([t.translate(self.translator) for t in self.tokens
                                    if t.wordform not in MorToken.punctuation or self.strip_punctuation is False])
        else:
            return "%s" % " ".join([str(t) for t in self.tokens
                                    if t.wordform not in MorToken.punctuation or self.strip_punctuation is False])

    def __repr__(self):
        return "MorSent[%s]" % " ".join([str(t) for t in self.tokens])


def sanitize_speaker_tier(line, debug=False, exampleConatainer=None):
    """
    this needs to be rewritten and documented.

    """

    # TODO:
    # - WHAT IS TH@IS?
    # - number sign (#) for pausing?
    # - +" = quotation preceeds
    # - currently just stripping out errors [* ... ]
    # - join lines ending with +... with lines starting with +,

    rewriteRules = [ # [pattern, rewrite, explanation]
        [r"^\*[A-Z0-9]+:[\s]+?", "", "strip speaker tier"],
        [r"\+<", "", "DON'T KNOW"],
        [r'\+,', r"", "erase interruption-completion marker"],
        [r"\(([^,.;?!<>\[\] ]+?)\)", r"\1", "remove omitted/incomplete word marking"],
        [r"<(.+?)> ?\[\?\]", r"\1", "remove best-guess marking on phrase"],
        [r"\[\?\]", r"", "remove best-guess marking on word"],
        # [r"", ""], # remove paralinguistic scoping and events
        [r"<([^<]+?)> \[(>\d*|<\d*)\]", r"\1", "remove overlap marking"],
        [r"<.+?> ?\[\/+\]", "", "remove (single) repitition of phrase"],
        [r"[^,.;?!<>\[\] ]+ \[\/+\]", "", "remove (single) repitition of word"],
        [r"<(.+?)> ?\[x \d+\]", r"\1", "remove phrasal multirepitition marking"],
        [r"([^,.;?!<>\[\] ]+) ?\[x \d+\]", r"\1", "remove word multirepitition marking"],
        [r"[^,.;?!<>\[\] ]+ *\[:(.*?)\]", r"\1", "transcriber-directed word replacement"],
        # it seems that mor does not ignore quotes
#        [r'(<.*?>|[^,.;?!<>\[\] ]+) \["\]', 'quote', "replace quotes w/ literal 'quote'"],
        [r"\[\^.+?\]", "", "not sure what these are. look like some kind of comment"],
        [r"\[\+.+?\]", "", "remove postcodes"],
        [r"\(\.+?\)", "", "remove pauses"],
        [r"\[\*.*?\]", "", "remove errors"],
        [r"&=([^,.;?!<>\[\] ]+)", r'', "remove events"],
        [r"<([^,.;?!<>\[\] ]+)>\s+\[=!.+?\]", r'\1', "remove scoped events"],
        [r"\[=!.+?\]", r'', "remove events"],
        [r"<([^,.;?!<>\[\] ]+)>\s+\[!!\]", r'\1', "remove scoped stressing"],
        [r"\[!!\]", r'', "remove stressing"],
        [r"\[=.+?\]", r'', "remove explanations"],
        [r"&\w+", "", "delete disfluencies"],
        [r"([^,.;?!<>\[\] ]+):([^,.;?!<>\[\] ]*)", r"\1\2", "remove exclamation markers"],
        [r"([^,.;?!<>\[\] ]+)@([a-z:*]{1,3})", r"\1", "remove special form markers"],
        #[r",", r" , "], # seperate commas
        [r'([^,.;?!<>\[\] ]+)-s', r"\1", "idk wtf -s means but it shows up"],
        [r'\+"', r"", "remove  quoted phrase marker"],
        [r"^\s*?,", '', "discard sentence-inital commas"],
        [r"^\s*?\+\+", "", "other persons completion marker"],
                         # sense. they can appear after things like
                         # [/] delete preceeding words
        #[r"\+\+|\+//", ""], # remove invited and self interuptions TODO: is this right?
        #[r"\+[./!]+(.)", r'\1'], # remove trailing offs and interuptions
        #[r"\(.*?\)|\[.*?\]", ""],

        # #    line = re.sub("[,.!?]", " ", line).rstrip('?!.+"/ \n')
        # #    line = re.sub(r"\b([a-z]\w+)('\w+)", r"\1 \2", line)
        # [r" +", " "],
        # [r"^[\W]+", ""],
        # [r" +", " "],
        # [r"^[\W]+", ""]
        ]
    [rule.append([]) for rule in rewriteRules]
    line = line.rstrip()
    matched = False
    for i in range(0, 3): # this is ridiculous but it means the rewrite
                         # rules will probably be exhausted. a while
                         # loop checking for non-consumed strings
                         # would be better but scares me.
        for num, (pattern, rewrite, explanation, examples) in enumerate(rewriteRules):
            if re.search(pattern, line):
                if debug:
                    matched = True
                    # print("PAT" + str(num), '\t', pattern, '--->', rewrite or "''")
                    rewriteRules[num][3].append("\t %s ---> %s" % \
                                                (line, re.sub(pattern, rewrite, line)))
                line = re.sub(pattern, rewrite, line)
                line = re.sub("\s+", " ", line).strip(' ')
        #if matched:
            #print('\t', line, '\n')

    if debug:
        for num, (pattern, rewrite, explanation, examples) in enumerate(rewriteRules):
            if examples:
                print(num, explanation, end='')
                [print('\t', example, end=' ') for example in examples]
                print()

    return line


def tagequiv_from_csv(filename):
    """ Creates a tagset equivalency list from a csv file

    args:
      `filename`: the name of the csv file to read.

    returns: a list of lists. each nested list contains a 'sparse
      mor_token' as its first element, and a pos tag as its second.
      a sparse mor token is a mor token with no required fields. a
      blank dict would count as a valid sparse token.

        [
          [{sparse-mor-token-dict}: 'rewrite-tag'],
          [{}, ''],
          ...
        ]

     the entries in filename must match the following regular
     expression: \w*?(:\w+?)*(&\w+?)*(-\w+?)(\|\w+)?,\w+?
       aka
                 pos:feature&fusion-suffix|wordform, newtag
       where all parts besides pos and |wordform can occur more than
       once, and newtag is arbitrary.

    """


    translation = []

    with open(filename) as cvsfile:
        lines = csv.reader(cvsfile)
        for pattern, newtag in lines:
            patternDict = {}
            parts = re.findall("(?:^|[:&-]|\|)\w+", pattern)
            priority = len(parts)
            patternDict['pos'] = parts[0]
            for part in parts[1:]:
                if part[0] == "|":
                    patternDict['lemma'] = part[1:]
                elif part[0] == ";":
                    patternDict[''] = part[1:]
                    patternDict['wordform'] = part[1:]
                else:
                    for symbol, key in [("&","fusions"), ("-", "suffixes"),
                                        (":", "features")]:
                        if part[0] == symbol:
                            if key not in patternDict:
                                patternDict[key] = []
                            patternDict[key].append(part[1:])
            translation.append((priority, patternDict, newtag))

    return [(i[1], i[2]) for i in
             sorted(translation, key=lambda x: x[0], reverse=True)]


def convert_mor_doc(filename, speaker=".*", not_speaker=None, translator=None, strip_punctuation=False):
    """
    Args:
        `filename`: the name of the chafile we're converting
        `callback`: a function for formatting each line of the
                    document.  callback is passed a list of mor token
                    dictionaries.
        `speaker`: the speaker to match. defaults to all speakers.
        `not_speaker`: match all speakers besides this one. can not be
                       passed with speaker

    Returns:
        None
    """

    # here we iterate through the file, collecting pairs of mor- and
    # speaker-tiers (we need to store the speaker tiers in order to
    # extract the original wordforms).
    if speaker is not None and not_speaker is not None:
        raise Exception("speaker and not_speaker arguments are mutually exclusive")
    with open(filename) as f:
        linenum = 0
        prevline = ""

        for num, line in collapse_lines("\t", f):
            mormatch = re.match(r"^%mor:.*", line)  # cur line is mor-tier
            # prev is speaker-tier
            if speaker:
                spkrmatch = re.match(r"^\*%s:.*" % speaker, prevline)
            else:
                spkrmatch = re.match(r"^\*(?!%s)[A-Z]{3}:.*" % not_speaker, prevline)
            if mormatch and spkrmatch:
                morline, speakerline = line.strip(), prevline.strip()
                if not re.match("^\S+$", morline): # don't parse blank lines
                    try:
                        sentence = MorSentence(speakerline, morline, translator, strip_punctuation)
                    except ValueError as error:
                        print('\t', speakerline, '\t', morline)
                        message = "%s \n\tLINE: %s \n\tFILE: %s" \
                                  % (error.message, linenum + 1, filename)

                        raise ValueError(message)
                    # except TagError as error:
                    #     print("parse not found:", error)
                    #     print(filename, "line:", linenum)
                    #     print('\t', speakerline, '\t', morline)
                    #     raise error
                    except (TagError, InvalidTagError) as error:
                        message = "%s \n\tLINE: %s \n\tFILE: %s" \
                                         % (error.message, linenum + 1, filename)
                        raise error.__class__(message, None)
                    yield sentence

            linenum += 1
            prevline = line

def split_nums(total, split):
    return int(math.floor(total * split)), int(math.ceil(total * (1 - split)))

def main(filename=None, directory=None, speaker=None, output_file=None,
         strip_punctuation=None, tagset_file=None, trainsize=None):

    speakerdict = {'child': r'CHI', 'adult': r'(?:COL|FAT|GLO|MOT|RIC|SAR|URS)',
                   'all': '.*'}


    if not (bool(filename) ^ bool(directory)):
        parser.error("You must include a filename or directory, exclusively")

    if directory is not None:
        filenames = glob.glob(os.path.join(directory, "*.cha"))
    else:
        filenames = [filename]

    translator = tagequiv_from_csv(tagset_file) if tagset_file else None
    strip_punctuation = strip_punctuation
    out = output_file
    if out is not None:
        lines = [line for filename in filenames
                for line in convert_mor_doc(filename, speakerdict[speaker], translator, strip_punctuation)]
        with nested(open(filename), \
                    open("%s-train.txt" % out, 'w'), open("%s-test.txt" % out, 'w')) \
                    as (f, trainfile, testfile):
            total = len(lines)
            trainlen, testlen = split_nums(total, trainsize)
            for line in lines[0:trainlen]:
                trainfile.write(line.__str__() + "\n")
            print(trainlen, "lines", "written to trainfile")
            for line in lines[trainlen:]:
                testfile.write(line.__str__() + "\n")
            print(testlen, "lines", "written to testfile")
    else:
        for filename in filenames:
            with open(filename) as f:
                for line in convert_mor_doc(filename, speakerdict[speaker], translator, strip_punctuation):
                    yield line

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('filename', nargs='?')
    parser.add_argument('-d', '--directory',
                        help='will run on all *.cha files in DIRECTORY')
    parser.add_argument('-s', '--speaker', choices=['child', 'adult', 'all'],
                        default='all',
                        help='only includes utterances by speaker of '\
                        'given category. default: %(default)s')
    parser.add_argument('-o', '--output-file',
                        help="output will be written to OUTPUT_FILE-train.txt " \
                        "and OUTPUT_FILE-test.txt")
    parser.add_argument('-p', '--strip-punctuation',
                        help='strip punctuation from output', action='store_true')
    parser.add_argument('-t', '--tagset-file',
                        help='use file TAGSET_FILE for tagset translation.')
    parser.add_argument('--trainsize', type=float, default=.9,
                        help='the percentage of the text to use for training. '\
                        'default: to %(default)s.')

    ARGS = parser.parse_args()
    args = vars(ARGS)
    for line in main(**args):
        print(line)
