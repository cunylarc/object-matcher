import unittest
from tokenmatch import *
from collections import namedtuple

class MatchTestFunctions(unittest.TestCase):
    def _isEmpty(self, iterable):
        try:
            iterable.next()
        except StopIteration:
            return True
        return False

    def _token_match_assertion(self, result, instance, num_results, token_length):
        result_list = list(result)
        self.assertEqual(len(result_list), num_results)
        for r in result_list:
            self.assertIsInstance(r, instance)
            if instance is not type(None):
                self.assertEqual(len(r.token_tail), token_length)

    def test_macro_expander(self):
        d = {"NP": "$NP V"}
        self.assertRaises(TokenCompilerException, make_matcher, "$NP V V")
        self.assertRaises(Warning, make_matcher, "$NP V V", d)
        #print(matcher.full_match(make_utterance("n/N")))

        # d["VP"] = "$NP V"
        # matcher = make_matcher("$NP", d)
        # matcher.full_match(make_utterance("n/N v/V"))

    def test_matcher(self):
        matcher = TokenMatcher(lemma="he", pos_list=["WH"])
        matcher = make_matcher("WH AUX PRO")
        utterance = make_utterance("he/WH is/AUX where/PRO")
        self.assertTrue(matcher.search(utterance))
        self.assertTrue(matcher.full_match(utterance))

    def test_token_matcher(self):

        matcher = TokenMatcher(lemma="what", pos_list=["WH"])
        result = matcher.match(make_utterance("what/WH"))
        self._token_match_assertion(result, MatchObject, 1, 0)

        matcher = TokenMatcher(lemma="hero")
        result = matcher.match(make_utterance("hero/N"))
        self._token_match_assertion(result, MatchObject, 1, 0)

        matcher = TokenMatcher(pos_list=["AUX"])
        result = matcher.match(make_utterance("is/AUX and/CONJ"))
        self._token_match_assertion(result, MatchObject, 1, 1)

        matcher = TokenMatcher(lemma="he")
        result = matcher.match(make_utterance("she/PRO"))
        self._token_match_assertion(result, None, 0, None)

        matcher = TokenMatcher(pos_list=["AUX"], exclude_pos=["WH"])
        result = matcher.match(make_utterance("is/AUX/WH"))
        self._token_match_assertion(result, None, 0, None)

        matcher = TokenMatcher(lemma="is", exclude_pos=["WH"])
        result = matcher.match(make_utterance("is/AUX/WH"))
        self._token_match_assertion(result, None, 0, None)

    def test_compile_token_matcher(self):
        for bad_token in ("!", "!!", "&", "&&", "POS&", "a&b", "a`PAST`", "a!bBANG",
                          "a`BANG", ".", ",", "?"):
            self.assertRaises(TokenCompilerException,
                              compile_token_matcher_expression, bad_token)

        matcher = compile_token_matcher_expression("is`wasAUX&EXIST!PRO")

        matcher = compile_token_matcher_expression("is`wasAUX&EXIST!PRO")
        self.assertEqual(matcher.lemma, "is")
        self.assertEqual(matcher.wordform, "was")
        self.assertEqual(matcher.pos_list, ["EXIST", "AUX"])
        self.assertEqual(matcher.exclude_pos, ["PRO"])

    def test_compile_token(self):
        #self.fail("You gotta add some more tests here buddy")
        for bad_token in ("&",):
            self.assertRaises(TokenCompilerException,
                              compile_token, bad_token)

        for good_token in (".", ",", "?", "!", "run!V"):
            compile_token(good_token)


    def test_class_matcher(self):
        # class matcher is deprecated. its functionality is a subset
        # of GroupMatcher. I've replaced all instances of ClassMatcher
        # and InvertedClasMatcher here with
        # GroupMatcher/InvertedGroupMatcher.

        matcher = GroupMatcher(TokenMatcher(pos_list=["WH"]))
        result = list(matcher.match(make_utterance("what/WH")))
        self.assertEqual(len(result), 1)
        self.assertIsInstance(result[0], MatchObject)

        matcher = GroupMatcher(TokenMatcher(lemma="what"),
                               TokenMatcher(pos_list=["WH"]))
        result = list(matcher.match(make_utterance("what/WH the/DET")))
        self.assertEqual(len(result), 2)
        for r in result:
            self.assertIsInstance(r, MatchObject)
            self.assertEqual(len(r.token_tail), 1)

        matcher = InvertedGroupMatcher(TokenMatcher(pos_list=["PRO"]))
        self.assertTrue(self._isEmpty(matcher.match(make_utterance("we/PRO"))))

        matcher = InvertedGroupMatcher(TokenMatcher(lemma="one"),
                                       TokenMatcher(pos_list=["ORD"]))
        self.assertTrue(self._isEmpty(matcher.match(make_utterance("two/ORD"
        " three/ORD"))))

        matcher = InvertedGroupMatcher(TokenMatcher(lemma="one"),
                                       TokenMatcher(pos_list=["ORD"]))
        self.assertTrue(matcher.match(make_utterance("one/NOPE")))


    def test_zero_or_one_matcher(self):
        matcher = ZeroOrOneMatcher(TokenMatcher(pos_list=["WH"]))
        utterance = make_utterance("what/WH is/AUX")
        results = list(matcher.match(utterance))
        for index, result in enumerate(results):
            self.assertEquals(len(result.token_tail), index + 1)

        results = list(matcher.match(make_utterance("is/AUX is/AUX")))
        self.assertEquals(len(results), 1)
        self.assertIsInstance(results[0], MatchObject)
        self.assertEquals(len(results[0].token_tail), 2)

    def test_range_matcher(self):
        matcher = RangeMatcher(TokenMatcher(pos_list=["WH"]))
        results = matcher._match(make_utterance("what/WH what/WH what/WH "
                                                "okay/YES"), 4)
        results = list(results)
        self.assertEquals(len(results), 3)
        for index, result in enumerate(results):
            self.assertEquals(len(result.token_tail), index + 1)

    def test_concat_matcher(self):
        matcher = ConcatenatedMatcher(OneOrMoreMatcher(TokenMatcher(
            pos_list=["A"])), GroupMatcher(TokenMatcher(pos_list=["B"])))

        for utterance_string in ("first/A first/A second/B third/X fourth/N",
                                 ("first/A first/A last/A and/A second/B"
                                  " third/X fourth/N")):
            utterance = make_utterance(utterance_string)
            result = list(matcher.match(utterance))
            self.assertEqual(len(result), 1)
            self.assertEqual(len(result[0].token_tail), 2)

        # S = (^V | AUX)* WH AUX .* ?
        matcher = ConcatenatedMatcher(
            ZeroOrMoreMatcher(
                InvertedGroupMatcher(TokenMatcher(pos_list=["V"]),
                                     TokenMatcher(pos_list=["AUX"]))),

            ConcatenatedMatcher(TokenMatcher(pos_list=["WH"]),
                                TokenMatcher(pos_list=["AUX"])))

        utterance = make_utterance("ha/EXL what/WH is/AUX this/XXX")
        result = list(matcher.match(utterance))

    def test_group_matcher(self):
        # (a b | c) d
        ab = ConcatenatedMatcher(TokenMatcher(pos_list=["A"]),
                                 TokenMatcher(pos_list=["B"]))
        c = TokenMatcher(pos_list=["C"])
        d = TokenMatcher(pos_list=["D"])
        abc = GroupMatcher(ab, c)
        matcher = ConcatenatedMatcher(abc, d)

        failers = ("a/A b/B c/C d/D", "d/D", "a/A")
        for s in failers:
            utterance = make_utterance(s)
            self.assertFalse(matcher.full_match(utterance))

        passers = ("a/A b/B d/D", "c/C d/D")
        for s in passers:
            utterance = make_utterance(s)
            self.assertTrue(matcher.full_match(utterance))

        matcher = concat(*[TokenMatcher(pos_list=[i]) for i in "ABCD"])
        result = list(matcher.match(make_utterance("a/A b/B c/C d/D")))
        self.assertEqual(len(result), 1)
        self.assertEqual(len(result[0].token_tail), 0)


    def test_inverted_group_matcher(self):
        ab = ConcatenatedMatcher(TokenMatcher(pos_list=["A"]),
                                 TokenMatcher(pos_list=["B"]))
        # A B
        c = TokenMatcher(pos_list=["C"])
        # C
        d = TokenMatcher(pos_list=["D"])
        # D
        abc = ZeroOrMoreMatcher(InvertedGroupMatcher(ab, c))
        # (^A B | C)
        matcher = ConcatenatedMatcher(abc, d)
        # (^A B | C) D

        passers = ("e/E d/D", "f/F d/D", "a/A d/D", "d/D", "a/A a/A d/D")
        for s in passers:
            utterance = make_utterance(s)
            self.assertTrue(matcher.full_match(utterance))

        failers = ("a/A b/B d/D", "c/C d/D", "a/A a/A", "a/A a/A b/B d/D")
        for s in failers:
            utterance = make_utterance(s)
            self.assertFalse(matcher.full_match(utterance))

        # matcher = concat(*[TokenMatcher(pos_list=[i]) for i in "ABCD"])
        # result = list(matcher.match(make_utterance("a/A b/B c/C d/D")))
        # self.assertEqual(len(result), 1)
        # self.assertEqual(len(result[0].token_tail), 0)


        # matcher = concat(*[TokenMatcher(pos_list=[i]) for i in "ABCD"])
        # result = list(matcher.match(make_utterance("a/A b/B c/C d/D")))
        # self.assertEqual(len(result), 1)
        # self.assertEqual(len(result[0].token_tail), 0)

    def test_full_match(self):
        self.longMessage = True
        passers = [("am/AUX what/WH", "am WH"),
                   ("i/PRO am/AUX what/WH", "i AUX? WH"),
                   ("i/PRO am/AUX what/WH", "PRO AUX WH"),
                   ("i/PRO am/AUX what/WH", "PRO AUX WH?"),
                   ("he/PRO what/WH", "PRO (^PRO)"),
                   ("i/PRO i/PRO", "i i"),
                   ("i/PRO i/PRO", "(PRO|WH) PRO"),
                   ]
        failers = [("i/PRO am/AUX what/WH/PRO", "i am WH!PRO"),
                   ("be/PRO/PAST", "beNOW&PRO!PAST"),
                   ("he/PRO he/PRO", "PRO (^PRO | WH)"),
                   ("he/PRO what/WH", "PRO (^PRO | WH)"),
                   ]

        for ustring, mstring in failers:
            utterance = make_utterance(ustring)
            matcher = make_matcher(mstring)
            self.assertFalse(any(i for i in matcher.match(utterance)), "Match should have failed")

        for ustring, mstring in passers:
            utterance = make_utterance(ustring)
            matcher = make_matcher(mstring)
            self.assertTrue(any(m.tail_len == 0
                            for m in matcher.match(utterance)), "Match failed")

