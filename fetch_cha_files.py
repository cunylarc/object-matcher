from __future__ import print_function
import os
import urllib2
from zipfile import ZipFile
from glob import glob
from os import path


DATA_DIR = "childes-data"
FILENAMES = ["Bates.zip", "Bloom70.zip", "Clark.zip", "Gleason.zip",
                 "Snow.zip", "Valian.zip"]

def fetch_data(base_url):
    if not os.path.exists(DATA_DIR):
        os.mkdir(DATA_DIR)

    for filename in FILENAMES:
        with open(path.join(DATA_DIR, filename), 'w') as fh:
            url = base_url + filename
            data = urllib2.urlopen(url)
            fh.write(data.read())
            print("downloaded", url)

def fetch_xml_data():
    fetch_data("http://childes.psy.cmu.edu/data-xml/Eng-USA/")

def fetch_cha_data():
    fetch_data("http://childes.psy.cmu.edu/data/Eng-USA/")

def unzip_files():
    for zfile in glob(path.join(DATA_DIR, "*.zip")):
        with ZipFile(zfile) as fh:
            fh.extractall(DATA_DIR)
            print("Unzipped", zfile)

if __name__ == "__main__":
    fetch_cha_data()
    unzip_files()
