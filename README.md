Syntax Overview
===============

syntax: ( ) (^ ) ? + * lowercase `lowercase UPPERCASE !UPPERCASE &UPPERCASE

Notation
--------

### lowercase

Will match a word with the lemma you've written in lowercase.

### `lowercase

Will match a word with the literal wordform you've written in
lowercase.

### UPPERCASE

Will match a word tagged with the uppercase string.
To require more than one part of speech, append additional UPPERCASE
pos-es using &s.

### !UPPERCASE

Will fail on a word that contains this POS. Can be appended like &.

### | : Or Operator

Will match one of a few options.

> A | B C | D

Will match if either A, B C or D matches.

### ( ) : Grouping

Any sequence of legal match strings can be grouped.

> (A | B)+ C

Matches any combination of As and Bs followed by a C.

### (^ ) : 'Anti-Grouping'

Will match only if none of the things inside the group match.

> (^A | D* Z) C

Matches [X] and then a C, where [X] is anything accept for A or zero
or more Ds followed by a Z.


### ... : Wildcard ###

Matches anything.

> A . B

Will match A immediately followed by anything, immediately followed by
B.

### *: Zero or More of the previous

> A B* C

Matches A followed by 0 or any number of Bs and then a C.

### +: One or More of the previous

> A B+ C

Matches A followed by 1 or any number of Bs and then a C.

### ? : Zero or One of the Previous  ###
Makes something optional

> A B? C

Will match A followed by an optional B and then a C.

