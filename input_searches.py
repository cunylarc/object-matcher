# -*- py-which-shell: "python2.7" -*-
from __future__ import print_function
import ConfigParser
import re
import os
import urllib2
import pdb
import pdb
import itertools
import pprint
from MorParser import convert_mor_doc
from collections import namedtuple, defaultdict
from functools import reduce
from glob import glob
from itertools import dropwhile
from os import path
from tokenmatch import TokenMatcher, GroupMatcher, ZeroOrOneMatcher, concat
from tokenmatch import make_utterance, make_matcher
from zipfile import ZipFile

DATA_DIR = "childes-data"
FILENAMES = ["Bates.zip", "Bloom70.zip", "Clark.zip", "Gleason.zip",
                 "Snow.zip", "Valian.zip"]

def gather_cha_files(directory):
    for dirpath, dirs, files in os.walk(directory):
        for f in files:
            if f.endswith(".cha"):
                yield path.join(dirpath, f)

def find_matches(sentence, match_list):
    for matcher in match_list:
        yield any(matcher.match(sentence))

def load_conf_file(filename):
    with open(filename) as fh:
        parser = ConfigParser.ConfigParser()
        parser.readfp(fh)
        defs = dict((k.upper(), v) for k,v in parser.items("definitions"))
        searches = dict((k, parser.get(k, 'search')) for k in parser.sections()
                         if k != "definitions")
        # searches = dict((k, parser.items[k]['search']) for k in parser.sections()
        #                 if k != "definitions")
        return (defs , searches)

def is_a_question(utterance):
    return utterance[-1].wordform == "?"

def log(corpus, section, word, sentence):
    fn = "%s-%s-%s.txt" % (corpus, section, word.replace("/", ""))
    fn = path.join("found-data", fn)
    try:
        fh = open(fn, 'a')
    except IOError:
        fh = open(fn, 'w')
    fh.write(str(sentence) + "\n")

def check_for_yn(sentence, search_string, defs, corpus_name, search_name,
                 analysis_dict, question_mark, verbose=False):
    utterance = list(sentence)
    if question_mark is False or is_a_question(utterance):
        if make_matcher(search_string, defs).full_match(utterance):
            log(corpus_name, search_name, "yes_no", sentence)
            analysis_dict[search_name][corpus_name]["yes/no"] += 1
            if verbose:
                print("[%s:%s:yes/no]" % (corpus_name, search_name),
                      str(sentence))


def check_for_wh(sentence, search_string, defs, corpus_name, search_name,
                 analysis_dict, question_mark, verbose=False):
    utterance = list(sentence)
    if question_mark is None or is_a_question(utterance):
        # tuple contains filename-component : search string replacment
        # pairs.
        for wh_type, wh_rewrite in (("what", "what"),
                                   ("which", "which ($NP)?"),
                                   ("when", "when"),
                                   ("why", "why")):

            if len(re.findall("<WH>", search_string)) > 1:
                raise Exception("You can't have more than one <WH> in a "
                                "search string.")
            if len(re.findall("<WH>", search_string)) < 1:
                raise Exception("You must include one <WH> in a wh "
                                "search-string")

            match_string = re.sub("<WH>", wh_rewrite, search_string)
            matcher = make_matcher(match_string, defs)
            if matcher.full_match(utterance):
                log(corpus_name, search_name, wh_type, sentence)
                analysis_dict[search_name][corpus_name][wh_type] += 1
                if verbose:
                    print("[%s:%s:%s]" % (corpus_name, search_name, wh_type),
                          str(sentence))



def check_for_question(sentence, wh, yns, defs, search_type,
                       corpus_name, analysis_dict, question_mark,
                       verbose=False):

    """ Performs seach queries on a sentence.

    If a match is found, write line to appropriate file and output
    line if vebose is True.

    Args
        wh: The WH search string to search for. This will be compiled as
            per tokenmatch.py. Additionally, this string should contain
            the substring"<WH>". For each wh-word what, which $NP?, when
            and why, a separate search will be compiled where"<WH>" is
            replace with the wh-word. If a match is found, the output
            file labeled with that wh-word will be written to.

        yn: A list of yn search strings to search for. This will be
            compiled without any of the additional preprocessing steps
            the WH undergoes.

        sentence: The thing to search against. A sequence of objects
                  that have wordform, lemma, pos and feature properties
                  defined on them.

        search_type: The name of the search. Will be used in the
                     filename to write data to. Filename is in the
                     format "corpus_name-search_name-wh.txt" where wh
                     is replaced with one of the four wh words, and
                     one ending in"-yn" that will be written to in the
                     case of a yn match

        corpus_name: The name of the corpus this search comes
                     from. Used in determining the filename to write
                     to.

        question_mark: A string- either "neither", "ys", "wh" or
                       "both". Specifies which of the two searches
                       requires there to be a question mark at the end
                       of the sentence.

        verbose: If True, will print matching line to stdout.

    """
    pass


def get_corpus_name(filename):
    last = None
    while True:
        ret= path.split(filename)
        if(ret[0]) == '':
            return last[1]
        filename = ret[0]
        last = ret

def read_corpora(searches, definitions):
    analysis_dict = {}
    search_types = ("inverted", "non-inverted", "non-inverted-all",
                    "inverted-all")
    search_words = ["what", "which", "why", "when", "yes/no"]
    corpora = ["Gleason", "Bloom70", "Bates", "Clark", "Snow", "Valian"]

    # search types -> search word -> corpus -> count
    analysis_dict = dict((stype, dict((corpus, defaultdict(int))
                                              for corpus in corpora))
                                 for stype in search_types)
    file_count = 0
    dirs = [d for d in glob(path.join(DATA_DIR, "*"))
            if path.isdir(d)]
    for d in dirs:
        corpus_files = list(gather_cha_files(d))
        for filename in corpus_files:
            file_count += 1
            # TODO: Rewrite using os.path
            corpus = get_corpus_name(filename)
            for num, sentence in enumerate(convert_mor_doc(filename, speaker=None,
                                                           not_speaker="CHI")):
                wh = searches['inverted wh questions']
                yn = searches['inverted yes/no questions']
                check_for_wh(sentence, wh, defs, corpus, "inverted",
                             analysis_dict, question_mark=True, verbose=False)
                check_for_yn(sentence, yn, defs, corpus, "inverted",
                             analysis_dict, question_mark=True, verbose=False)

                wh = searches['non-inverted wh questions']
                yn = searches['non-inverted yes/no questions']
                check_for_wh(sentence, wh, defs, corpus, "non-inverted",
                             analysis_dict, question_mark=False, verbose=False)
                check_for_yn(sentence, yn, defs, corpus, "non-inverted",
                             analysis_dict, question_mark=True, verbose=False)

                wh = searches['non-inverted wh all-forms']
                yn1 = searches['inverted yes/no all-forms']
                yn2 = searches['non-inverted yes/no all-forms']
                check_for_wh(sentence, wh, defs, corpus, "non-inverted-all",
                             analysis_dict, question_mark=False, verbose=False)
                check_for_yn(sentence, yn1, defs, corpus, "inverted-all",
                             analysis_dict, question_mark=False, verbose=False)
                check_for_yn(sentence, yn2, defs, corpus, "non-inverted-all",
                             analysis_dict, question_mark=False, verbose=False)
            print("#%s" % file_count, corpus, filename)

    for search_type, words in analysis_dict.items():
        print(search_type.upper())
        for word, counts in words.items():
            print("\t", word)
            for corpus, count in counts.items():
                print("\t\t%s: %s" % (corpus, count))

if __name__ == "__main__":
    try:
        defs, searches = load_conf_file("searches.conf")
    except ConfigParser.ParsingError:
        raise Exception(("Couldn't parse your search file. Make sure there's "
                         "no whitespace at he beginning of your lines"))
    allowed_searches = ['non-inverted yes/no questions',
                        'inverted yes/no all-forms',
                        'non-inverted wh all-forms',
                        'inverted yes/no questions',
                        'non-inverted wh questions',
                        'inverted wh questions',
                        'non-inverted yes/no all-forms']
    if len(set(searches.keys()).difference(set(allowed_searches))) > 0:
        bad_ones = set(searches.keys()).difference(set(allowed_searches))
        raise Exception("Unrecognized search names: %s" % ", ".join(bad_ones))
    read_corpora(searches, defs)
